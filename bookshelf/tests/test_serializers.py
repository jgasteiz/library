import unittest

from bookshelf.models import Book
from bookshelf.serializers import BookSerializer
from django.test import TestCase


class BookSerializerTests(TestCase):
    def setUp(self):
        self.book = Book.objects.create(
            title='Batman: The Killing Joke',
            author='Alan Moore, Brian Bolland',
            image='http://i.imgur.com/BU1sKzS.jpg'
        )

    def test_create(self):
        serializer = BookSerializer(instance=self.book)
        self.assertEqual(serializer.data.get('title'), 'Batman: The Killing Joke')
        self.assertEqual(serializer.data.get('author'), 'Alan Moore, Brian Bolland')
        self.assertEqual(serializer.data.get('image'), 'http://i.imgur.com/BU1sKzS.jpg')
        self.assertEqual(serializer.data.get('amazon_link'), 'https://www.amazon.co.uk/s/?field-keywords=Batman: The Killing Joke')
