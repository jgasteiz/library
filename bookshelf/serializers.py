from bookshelf.models import Book
from django.contrib.auth.models import User
from rest_framework import routers, serializers, viewsets


class BookSerializer(serializers.ModelSerializer):
    amazon_link = serializers.SerializerMethodField()

    class Meta:
        model = Book
        fields = ('id', 'title', 'author', 'image', 'in_wishlist', 'amazon_link',)

    def get_amazon_link(self, obj):
        return 'https://www.amazon.co.uk/s/?field-keywords={} {}'.format(obj.title, obj.author)


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', 'is_superuser')
        read_only_fields = ('is_staff', 'is_superuser', 'is_active', 'date_joined',)
