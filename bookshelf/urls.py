from django.conf.urls import url, include
from rest_framework import routers
from bookshelf import views

router = routers.DefaultRouter()
router.register(r'books', views.BookViewSet)


urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^api/current_user/', views.current_user, name='current_user'),
    url(r'^api/search/', views.search, name='search'),
    url(r'^api/', include(router.urls)),
]
