from natsort import natsorted, ns

from django.db import models


class BookManager(models.Manager):
    def sorted_by_title(self):
        return natsorted(self.all(), key=lambda x: x.title.lower())


class Book(models.Model):
    title = models.CharField(max_length=256)
    author = models.CharField(max_length=256)
    image = models.URLField(blank=True, max_length=1024)
    in_wishlist = models.BooleanField(default=False)
    objects = BookManager()
