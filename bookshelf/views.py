import requests
from bookshelf.models import Book
from bookshelf.serializers import BookSerializer, UserSerializer
from django.conf import settings
from django.views.generic import TemplateView
from rest_framework import (
    permissions,
    views,
    viewsets,
)
from rest_framework.decorators import api_view
from rest_framework.response import Response


class HomeView(TemplateView):
    template_name = 'bookshelf/index.html'

home = HomeView.as_view()


class SearchView(views.APIView):
    def get(self, request):
        query = request.GET.get('query')
        api_base_url = 'http://comicvine.gamespot.com/api/search/?api_key={}&format=json&resources=volume&limit=20'.format(settings.VINE_API_KEY)

        if query:
            api_url = '{}&query={}'.format(api_base_url, query)
            headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36'}
            response = requests.get(api_url, headers=headers)
            return Response(response.json())

        return Response({})

search = SearchView.as_view()


class BookViewSet(viewsets.ModelViewSet):
    queryset = Book.objects.all()
    serializer_class = BookSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)

    def list(self, request, *args, **kwargs):
        queryset = Book.objects.sorted_by_title()
        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


@api_view(['GET'])
def current_user(request):
    serializer = UserSerializer(request.user)
    return Response(serializer.data)
