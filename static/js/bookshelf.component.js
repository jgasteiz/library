"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var book_form_component_1 = require('./book-form.component');
var book_list_component_1 = require('./book-list.component');
var book_1 = require('./book');
var library_service_1 = require('./library.service');
var Bookshelf = (function () {
    function Bookshelf(libraryService) {
        this.libraryService = libraryService;
        this.newBook = new book_1.BookModel();
    }
    Bookshelf.prototype.ngOnInit = function () {
        this.libraryService.getBooks();
        this.libraryService.getUser();
    };
    /**
     * Check whether the current user can create a new book or not.
     * @returns {boolean}
     */
    Bookshelf.prototype.canCreateBook = function () {
        return this.libraryService.isSuperuser;
    };
    /**
     * Open the book creation form.
     */
    Bookshelf.prototype.openBookForm = function () {
        this.newBook.showBookForm = true;
        componentHandler.upgradeDom();
    };
    Bookshelf = __decorate([
        core_1.Component({
            selector: 'bookshelf',
            directives: [book_list_component_1.BookList, book_form_component_1.BookForm],
            templateUrl: '/static/templates/bookshelf.component.html'
        }), 
        __metadata('design:paramtypes', [library_service_1.LibraryService])
    ], Bookshelf);
    return Bookshelf;
}());
exports.Bookshelf = Bookshelf;
