"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var library_service_1 = require('./library.service');
var book_1 = require('./book');
var book_form_component_1 = require('./book-form.component');
var Book = (function () {
    function Book(libraryService) {
        this.libraryService = libraryService;
    }
    /**
     * Open the book form.
     */
    Book.prototype.openBookForm = function () {
        this.book.showBookForm = true;
        componentHandler.upgradeDom();
    };
    /**
     * Check if the current user has permission for managing the book.
     * @returns {boolean}
     */
    Book.prototype.canManageBook = function () {
        return this.libraryService.isSuperuser;
    };
    /**
     * Delete the book.
     */
    Book.prototype.deleteBook = function () {
        if (confirm('Are you sure you want to delete this book? ' + this.book.title)) {
            this.libraryService.deleteBook(this.book);
        }
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', book_1.BookModel)
    ], Book.prototype, "book", void 0);
    Book = __decorate([
        core_1.Component({
            selector: 'book',
            directives: [book_form_component_1.BookForm],
            templateUrl: '/static/templates/book.component.html'
        }), 
        __metadata('design:paramtypes', [library_service_1.LibraryService])
    ], Book);
    return Book;
}());
exports.Book = Book;
