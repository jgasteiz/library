"use strict";
var platform_browser_dynamic_1 = require('@angular/platform-browser-dynamic');
var http_1 = require('@angular/http');
var core_1 = require('@angular/core');
var bookshelf_component_1 = require('./bookshelf.component');
var library_service_1 = require('./library.service');
var utils_1 = require('./utils');
platform_browser_dynamic_1.bootstrap(bookshelf_component_1.Bookshelf, [
    http_1.HTTP_PROVIDERS,
    core_1.provide(http_1.RequestOptions, { useClass: utils_1.CustomRequestOptions }),
    library_service_1.LibraryService
]);
