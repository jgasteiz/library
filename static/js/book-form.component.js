"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var library_service_1 = require('./library.service');
var book_1 = require('./book');
var BookForm = (function () {
    function BookForm(libraryService) {
        this.libraryService = libraryService;
    }
    /**
     * Save the book or create a new book.
     */
    BookForm.prototype.save = function () {
        this.book.showBookForm = false;
        this.libraryService.saveBook(this.book);
        this.resetForm();
    };
    /**
     * Close the form.
     */
    BookForm.prototype.cancel = function () {
        this.book.showBookForm = false;
        this.resetForm();
    };
    /**
     * Reset the form instance.
     */
    BookForm.prototype.resetForm = function () {
        if (!this.book.id) {
            this.book.reset();
        }
    };
    __decorate([
        core_1.Input(), 
        __metadata('design:type', book_1.BookModel)
    ], BookForm.prototype, "book", void 0);
    BookForm = __decorate([
        core_1.Component({
            selector: 'book-form',
            templateUrl: '/static/templates/book-form.component.html'
        }), 
        __metadata('design:paramtypes', [library_service_1.LibraryService])
    ], BookForm);
    return BookForm;
}());
exports.BookForm = BookForm;
