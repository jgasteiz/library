"use strict";
var BookModel = (function () {
    function BookModel(id, title, author, in_wishlist, amazon_link, image, showBookForm) {
        if (id === void 0) { id = 0; }
        if (title === void 0) { title = ''; }
        if (author === void 0) { author = ''; }
        if (in_wishlist === void 0) { in_wishlist = false; }
        if (amazon_link === void 0) { amazon_link = ''; }
        if (image === void 0) { image = ''; }
        if (showBookForm === void 0) { showBookForm = false; }
        this.id = id;
        this.title = title;
        this.author = author;
        this.in_wishlist = in_wishlist;
        this.amazon_link = amazon_link;
        this.image = image;
        this.showBookForm = showBookForm;
    }
    /**
     * Reset the book properties to their initial values.
     */
    BookModel.prototype.reset = function () {
        this.title = '';
        this.author = '';
        this.in_wishlist = false;
        this.amazon_link = '';
        this.image = '';
        this.showBookForm = false;
    };
    return BookModel;
}());
exports.BookModel = BookModel;
