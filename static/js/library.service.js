"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var book_1 = require('./book');
require('rxjs/Rx');
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
var utils_1 = require('./utils');
var LibraryService = (function () {
    function LibraryService(http) {
        this.http = http;
        this.books = [];
        this.isSuperuser = false;
        /**
         * Get the current user
         */
        this.getUser = function () {
            var _this = this;
            this.http.get('/api/current_user/')
                .map(function (response) { return response.json(); })
                .subscribe(function (data) { return _this.isSuperuser = data['is_superuser'] || false; }, function (err) { return console.error(err); }, function () { return console.log('Random Quote Complete'); });
        };
        /**
         * Initialize all the books.
         */
        this.getBooks = function () {
            var _this = this;
            this.http.get(this._getBooksAPIUrl(null))
                .map(function (response) { return response.json(); })
                .subscribe(function (data) {
                _this.books = data.map(function (item) {
                    return new book_1.BookModel(item['id'] || 0, item['title'] || '', item['author'] || '', item['in_wishlist'] || false, item['amazon_link'] || '', item['image'] || '');
                });
            }, function (err) { return console.error(err); }, function () { return console.log('Random Quote Complete'); });
        };
        /**
         * Save a given existing book or create a new one if it doesn't have an id.
         * @param book
         */
        this.saveBook = function (book) {
            if (book.id) {
                this.updateBook(book);
            }
            else {
                this.createBook(book);
            }
        };
        /**
         * Create a book with the given properties.
         * @param book
         */
        this.createBook = function (book) {
            var _this = this;
            this.http.post(this._getBooksAPIUrl(book), JSON.stringify(book), this._getRequestOptions())
                .map(function (response) { return response.json(); })
                .subscribe(function (data) {
                _this.books.push(new book_1.BookModel(data['id'] || 0, data['title'] || '', data['author'] || '', data['in_wishlist'] || false, data['amazon_link'] || '', data['image'] || ''));
            }, function (err) { return console.error(err); }, function () { return console.log('Book created'); });
        };
        /**
         * Update a given book.
         * @param book
         */
        this.updateBook = function (book) {
            this.http.put(this._getBooksAPIUrl(book), JSON.stringify(book), this._getRequestOptions())
                .map(function (response) { return response.json(); })
                .subscribe(function (data) {
                book = new book_1.BookModel(data['id'] || 0, data['title'] || '', data['author'] || '', data['in_wishlist'] || false, data['amazon_link'] || '', data['image'] || '');
            }, function (err) { return console.error(err); }, function () { return console.log('Book updated'); });
        };
        /**
         * Delete a book.
         * @param book
         */
        this.deleteBook = function (book) {
            var _this = this;
            this.http.delete(this._getBooksAPIUrl(book), this._getRequestOptions())
                .subscribe(function (res) {
                _this.getBooks();
            }, function (err) { return console.error(err); }, function () { return console.log('Book deleted'); });
        };
        /**
         * Get the API endpoint depending on if there's a book to create, update
         * or delete.
         * @param book
         * @returns {any}
         * @private
         */
        this._getBooksAPIUrl = function (book) {
            if (book && book.id) {
                return '/api/books/' + book.id + '/';
            }
            else {
                return '/api/books/';
            }
        };
        /**
         * Return the request options for PUT, POST and DELETE requests.
         * @returns {CustomRequestOptions}
         * @private
         */
        this._getRequestOptions = function () {
            var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
            return new utils_1.CustomRequestOptions({ headers: headers });
        };
    }
    LibraryService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], LibraryService);
    return LibraryService;
}());
exports.LibraryService = LibraryService;
