
export class BookModel {
    constructor(
        public id:number = 0,
        public title:string = '',
        public author:string = '',
        public in_wishlist:boolean = false,
        public amazon_link:string = '',
        public image:string = '',
        public showBookForm:boolean = false
    ) {}

    /**
     * Reset the book properties to their initial values.
     */
    reset() {
        this.title = '';
        this.author = '';
        this.in_wishlist = false;
        this.amazon_link = '';
        this.image = '';
        this.showBookForm = false;
    }
}
