import {Headers, BaseRequestOptions} from '@angular/http';

export class CustomRequestOptions extends BaseRequestOptions {
    constructor(options:Object) {
        super();
        if (options) {
            this.headers = options['headers'];
        }
        this.headers.append('X-CSRFToken', this.getCookie('csrftoken'));
    }

    getCookie(name: string) {
        let value = "; " + document.cookie;
        let parts = value.split("; " + name + "=");
        if (parts.length === 2) {
            return parts.pop().split(";").shift();
        }
    }
}
