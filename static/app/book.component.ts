import {Component, Input} from '@angular/core';
import {LibraryService} from './library.service';
import {BookModel} from './book';
import {BookForm} from './book-form.component';

declare var componentHandler: any;

@Component({
    selector: 'book',
    directives: [BookForm],
    templateUrl: '/static/templates/book.component.html'
})
export class Book {
    @Input() book:BookModel;

    constructor(public libraryService:LibraryService) {}

    /**
     * Open the book form.
     */
    openBookForm() {
        this.book.showBookForm = true;
        componentHandler.upgradeDom();
    }

    /**
     * Check if the current user has permission for managing the book.
     * @returns {boolean}
     */
    canManageBook() {
        return this.libraryService.isSuperuser;
    }

    /**
     * Delete the book.
     */
    deleteBook() {
        if (confirm('Are you sure you want to delete this book? ' + this.book.title)) {
            this.libraryService.deleteBook(this.book);
        }
    }
}
