import {BookModel} from './book';
import {Component, Input} from '@angular/core';
import {LibraryService} from './library.service';
import {Book} from './book.component';

@Component({
    selector: 'book-list',
    directives: [Book],
    templateUrl: '/static/templates/book-list.component.html'
})
export class BookList {
    @Input() listType:String;

    constructor(public libraryService:LibraryService) {}

    isBookHidden(book:BookModel) {
        return (book.in_wishlist && this.listType == 'bookshelf') || (!book.in_wishlist && this.listType == 'wishlist');
    }
}
