import {BookModel} from './book';
import 'rxjs/Rx';
import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';
import {CustomRequestOptions} from './utils';

@Injectable()
export class LibraryService {

    books:BookModel[] = [];
    isSuperuser:boolean = false;

    constructor(public http:Http) {}

    /**
     * Get the current user
     */
    getUser = function () {
        this.http.get('/api/current_user/')
            .map((response:any) => response.json())
            .subscribe(
                (data:Object) => this.isSuperuser = data['is_superuser'] || false,
                (err:any) => console.error(err),
                () => console.log('Random Quote Complete')
            );
    };

    /**
     * Initialize all the books.
     */
    getBooks = function () {
        this.http.get(this._getBooksAPIUrl(null))
            .map((response:any) => response.json())
            .subscribe(
                (data:Object[]) => {
                    this.books = data.map(function (item) {
                        return new BookModel(
                            item['id'] || 0,
                            item['title'] || '',
                            item['author'] || '',
                            item['in_wishlist'] || false,
                            item['amazon_link'] || '',
                            item['image'] || ''
                        )
                    });
                },
                (err:any) => console.error(err),
                () => console.log('Random Quote Complete')
            );
    };

    /**
     * Save a given existing book or create a new one if it doesn't have an id.
     * @param book
     */
    saveBook = function (book:BookModel) {
        if (book.id) {
            this.updateBook(book);
        } else {
            this.createBook(book);
        }
    };

    /**
     * Create a book with the given properties.
     * @param book
     */
    createBook = function (book:BookModel) {
        this.http.post(this._getBooksAPIUrl(book), JSON.stringify(book), this._getRequestOptions())
            .map((response:any) => response.json())
            .subscribe(
                (data:Object) => {
                    this.books.push(new BookModel(
                        data['id'] || 0,
                        data['title'] || '',
                        data['author'] || '',
                        data['in_wishlist'] || false,
                        data['amazon_link'] || '',
                        data['image'] || ''
                    ));
                },
                (err:any) => console.error(err),
                () => console.log('Book created')
            );
    };

    /**
     * Update a given book.
     * @param book
     */
    updateBook = function (book:BookModel) {
        this.http.put(this._getBooksAPIUrl(book), JSON.stringify(book), this._getRequestOptions())
            .map((response:any) => response.json())
            .subscribe(
                (data:Object) => {
                    book = new BookModel(
                        data['id'] || 0,
                        data['title'] || '',
                        data['author'] || '',
                        data['in_wishlist'] || false,
                        data['amazon_link'] || '',
                        data['image'] || ''
                    );
                },
                (err:any) => console.error(err),
                () => console.log('Book updated')
            );
    };

    /**
     * Delete a book.
     * @param book
     */
    deleteBook = function (book:BookModel) {
        this.http.delete(this._getBooksAPIUrl(book), this._getRequestOptions())
            .subscribe(
                (res:any) => {
                    this.getBooks();
                },
                (err:any) => console.error(err),
                () => console.log('Book deleted')
            );
    };

    /**
     * Get the API endpoint depending on if there's a book to create, update
     * or delete.
     * @param book
     * @returns {any}
     * @private
     */
    _getBooksAPIUrl = function (book:BookModel) {
        if (book && book.id) {
            return '/api/books/' + book.id + '/';
        } else {
            return '/api/books/';
        }
    };

    /**
     * Return the request options for PUT, POST and DELETE requests.
     * @returns {CustomRequestOptions}
     * @private
     */
    _getRequestOptions = function () {
        let headers = new Headers({'Content-Type': 'application/json'});
        return new CustomRequestOptions({headers: headers});
    };
}
