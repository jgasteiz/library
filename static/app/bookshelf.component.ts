import {Component} from '@angular/core';
import {BookForm} from './book-form.component';
import {BookList} from './book-list.component';
import {BookModel} from './book';
import {LibraryService} from './library.service';

declare var componentHandler: any;

@Component({
    selector: 'bookshelf',
    directives: [BookList, BookForm],
    templateUrl: '/static/templates/bookshelf.component.html'
})
export class Bookshelf {
    public newBook:BookModel;

    constructor(public libraryService: LibraryService) {
        this.newBook = new BookModel();
    }

    ngOnInit() {
        this.libraryService.getBooks();
        this.libraryService.getUser();
    }

    /**
     * Check whether the current user can create a new book or not.
     * @returns {boolean}
     */
    canCreateBook() {
        return this.libraryService.isSuperuser;
    }

    /**
     * Open the book creation form.
     */
    openBookForm () {
        this.newBook.showBookForm = true;
        componentHandler.upgradeDom();
    }
}
