import {bootstrap}    from '@angular/platform-browser-dynamic';
import {HTTP_PROVIDERS, RequestOptions} from '@angular/http';
import {provide} from '@angular/core';
import {Bookshelf} from './bookshelf.component';
import {LibraryService} from './library.service';
import {CustomRequestOptions} from './utils';

bootstrap(Bookshelf, [
    HTTP_PROVIDERS,
    provide(RequestOptions, {useClass: CustomRequestOptions}),
    LibraryService
]);
