import {Component, Input} from '@angular/core';
import {LibraryService} from './library.service';
import {BookModel} from './book';

@Component({
    selector: 'book-form',
    templateUrl: '/static/templates/book-form.component.html'
})
export class BookForm {
    @Input() book:BookModel;

    constructor(public libraryService:LibraryService) {}

    /**
     * Save the book or create a new book.
     */
    save() {
        this.book.showBookForm = false;
        this.libraryService.saveBook(this.book);
        this.resetForm();
    }

    /**
     * Close the form.
     */
    cancel() {
        this.book.showBookForm = false;
        this.resetForm();
    }

    /**
     * Reset the form instance.
     */
    resetForm() {
        if (!this.book.id) {
            this.book.reset();
        }
    }
}
