'use strict';

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    ts = require('gulp-typescript'),
    tsProject = ts.createProject('tsconfig.json');

gulp.task('sass', function () {
    return gulp.src('./static/scss/**/*.scss')
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(gulp.dest('./static/css'));
});

gulp.task('copy', function() {
    // Copy material.min.js
    gulp.src('./node_modules/material-design-lite/material.min.js')
        .pipe(gulp.dest('./static/js'));
});

gulp.task('scripts', function() {
    var tsResult = tsProject.src()
        .pipe(ts(tsProject));

    return tsResult.js.pipe(gulp.dest('static/js'));
});

gulp.task('watch', function () {
    gulp.watch('./static/scss/**/*', ['sass']);
    gulp.watch('./static/app/**/*', ['scripts']);
});

gulp.task('default', ['copy', 'scripts', 'sass', 'watch']);
